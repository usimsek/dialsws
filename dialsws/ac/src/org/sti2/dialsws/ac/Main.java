package org.sti2.dialsws.ac;


import com.github.jsonldjava.core.JsonLdError;
import org.json.simple.parser.ParseException;
import org.sti2.dialsws.ac.org.sti2.dialsws.ac.collector.Collector;

import java.io.IOException;
import java.net.URISyntaxException;

public class Main {

    public static void main(String args[])
    {
        try {

            Collector collector = new Collector("config.json", new KBManager());
            collector.downloadAnnotations();

            collector.pushAllToKB();

        } catch (IOException | JsonLdError | ParseException | URISyntaxException e) {
            e.printStackTrace();
        }
    }


}
