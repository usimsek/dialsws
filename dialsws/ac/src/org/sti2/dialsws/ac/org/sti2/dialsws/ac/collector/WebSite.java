package org.sti2.dialsws.ac.org.sti2.dialsws.ac.collector;

public class WebSite {
    private String graphName;
    private String apiKey;
    private boolean ignore = false;

    public String getGraphName() {
        return graphName;
    }

    public void setGraphName(String graphName) {
        this.graphName = graphName;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public boolean isIgnore() {
        return ignore;
    }

    public void setIgnore(boolean ignore) {
        this.ignore = ignore;
    }

    public WebSite (String graphName, String apiKey, boolean ignore)
    {
        this.graphName = graphName;

        this.apiKey = apiKey;

        this.ignore = ignore;
    }

}
