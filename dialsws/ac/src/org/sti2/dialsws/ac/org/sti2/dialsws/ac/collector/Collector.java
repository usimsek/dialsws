package org.sti2.dialsws.ac.org.sti2.dialsws.ac.collector;

import com.github.jsonldjava.core.JsonLdError;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.sti2.dialsws.ac.KBManager;
import org.sti2.dialsws.client.SemantifyItClient;

import java.io.*;
import java.net.URISyntaxException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;

public class Collector {

    private String cachePath;
    private ArrayList<WebSite> websites;
    //private String errorPath;
    //private String queryErrors;
    private String apiPath;
    private KBManager kbManager;

    public Collector(String configPath, KBManager kbManager) throws IOException, ParseException, URISyntaxException {
        websites = new ArrayList<WebSite>();
        this.kbManager = new KBManager();
        loadConfig(configPath);

    }

    public void downloadAnnotations() throws URISyntaxException, IOException {


        for (WebSite webSite : websites) {

            if (isLastDownloadExpired(webSite.getApiKey())) {

                SemantifyItClient client = new SemantifyItClient();
                JSONArray hashes = client.getAnnotationHashList(webSite.getApiKey());
                JSONArray annotations = new JSONArray();

                long remaining = hashes.size();
                System.out.println("Remaining annotations: " + remaining);
                for (Object anno : hashes
                        ) {

                    ArrayList<JSONObject> jsons = client.getSingleAnnotation(((JSONObject) anno).get("UID").toString());

                    while (jsons == null) {

                        jsons = client.getSingleAnnotation(((JSONObject) anno).get("UID").toString());
                    }

                    System.out.println("Remaining annotations: " + remaining--);

                    for (JSONObject jobj : jsons) annotations.add(jobj);

                }


                FileWriter fileWriter = new FileWriter(cachePath+webSite.getApiKey() + ".json");
                annotations.writeJSONString(fileWriter);
                //fileWriter.write(annotations.writeJSONString());
                fileWriter.flush();
                fileWriter.close();
            }
        }
    }

    public void pushAllToKB() throws URISyntaxException, JsonLdError, ParseException, IOException {

        kbManager.cleanKB(websites);

        for (WebSite webSite : websites)
            kbManager.pushToKB(cachePath+webSite.getApiKey()+".json", webSite.getGraphName());

    }

    private boolean isLastDownloadExpired(String apiKey) {

        File f = new File(cachePath+apiKey+".json");

        if (!f.exists())
            return true;

        LocalDate lm = Instant.ofEpochMilli(f.lastModified()).atZone(ZoneId.systemDefault()).toLocalDate();

        if (lm.compareTo(LocalDate.now()) < 0)
            return true;

        return false;


    }

    private void loadConfig(String configPath) throws IOException, ParseException {

        JSONParser parser = new JSONParser();
        JSONObject json = (JSONObject) parser.parse(new FileReader(configPath));
        this.cachePath = json.get("cachePath").toString();
        this.apiPath = json.get("apiPath").toString();

        JSONArray jWebsites = (JSONArray) json.get("websites");

        for (Object jWebsite : jWebsites) {

            JSONObject jsonWebsite = (JSONObject) jWebsite;

            if (!(boolean)jsonWebsite.get("ignore"))
                websites.add(new WebSite(jsonWebsite.get("graphName").toString(),jsonWebsite.get("apiKey").toString(),(boolean)jsonWebsite.get("ignore")));
        }

       // this.errorPath = json.get("errorPath").toString();
        //this.queryErrors = json.get("sparqlErrorPath").toString();

    }

}
