package org.sti2.dialsws.ac;


import com.github.jsonldjava.core.*;
import org.apache.commons.io.FileUtils;
import org.apache.jena.datatypes.xsd.XSDDatatype;
import org.apache.jena.graph.Graph;
import org.apache.jena.graph.Node;
import org.apache.jena.graph.NodeFactory;
import org.apache.jena.graph.Triple;
import org.apache.jena.query.Dataset;
import org.apache.jena.query.DatasetFactory;
import org.apache.jena.query.QueryParseException;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.sparql.core.DatasetGraph;
import org.apache.jena.sparql.core.DatasetGraphFactory;
import org.apache.jena.sparql.graph.GraphFactory;
import org.apache.jena.update.UpdateAction;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.sti2.at.dialsws.client.GraphDBClient;
import org.sti2.dialsws.ac.org.sti2.dialsws.ac.collector.WebSite;

import java.io.*;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class KBManager {

    private GraphDBClient client ;

    public KBManager () throws URISyntaxException {
        client = new GraphDBClient("http://graphdb.sti2.at:8080");
    }

    private JSONArray readFromFile(String fileName) throws IOException, ParseException {
        JSONParser parser = new JSONParser();
        JSONArray json = (JSONArray) parser.parse(new FileReader(fileName));
        return json;
    }

    public void convertToNQuad(JSONArray json, String graphName) throws JsonLdError, IOException, URISyntaxException {

        DocumentLoader dl = new DocumentLoader();
        JsonLdOptions options = new JsonLdOptions();

        String context = FileUtils.readFileToString(new File("resources/schema.jsonld"), Charset.forName("UTF-8"));

        dl.addInjectedDoc("http://schema.org", context);
        dl.addInjectedDoc("http://schema.org/", context);
        dl.addInjectedDoc("https://schema.org", context);
        dl.addInjectedDoc("https://schema.org/", context);
        options.setDocumentLoader(dl);

        ArrayList<Graph> ga = new ArrayList<Graph>();

        int i= 0;
        while (i<json.size()) {
            try {
                Graph g = GraphFactory.createDefaultGraph();
                preprocess((JSONObject) json.get(i), g);

                RDFDataset dataset = (RDFDataset) JsonLdProcessor.toRDF((JSONObject) json.get(i), options);
                List<RDFDataset.Quad> quads = dataset.getQuads("@default");

                for (RDFDataset.Quad q : quads) {

                    Node s = createNode(q.getSubject());
                    Node p = createNode(q.getPredicate());
                    Node o = createNode(q.getObject());
                    //Node gr = NodeFactory.createURI(graphName);


                    Triple t = Triple.create(s, p, o);
                    g.add(t);

                }
                ga.add(g);
                int stmntCount = sumStatements(ga, graphName);
                System.out.println(stmntCount);
                if (stmntCount > 4000 || i == json.size() - 1) {
                    DatasetGraph dg = mergeGraphs(ga, graphName);
                    StringWriter out = new StringWriter();
                    RDFDataMgr.write(out, dg, Lang.NQUADS);
                    System.out.println(out.toString());
                    client.pushNquadsToGraphDB(out.toString());
                    ga.clear();

                }

                i++;
            }
            catch (Exception ex)
            {
                ex.printStackTrace();
            }

        }

    }

    private DatasetGraph mergeGraphs(ArrayList<Graph> ga, String graphName) throws IOException {

        DatasetGraph dg = DatasetGraphFactory.create();
        Dataset ds = DatasetFactory.wrap(dg);

        for (Graph g : ga) {
            String insertQuery="";
            try {
                StringWriter out = new StringWriter();
                RDFDataMgr.write(out, g, Lang.TURTLE);
                insertQuery = "INSERT DATA {GRAPH <" + graphName + "> {" + out.toString() + "}}";
                UpdateAction.parseExecute(insertQuery, ds);
            }
            catch (QueryParseException qpe)
            {
                FileWriter fileWriter = new FileWriter("queryErrors"+new Date().getTime()+".txt");
                fileWriter.write(insertQuery);
                fileWriter.flush();
            }
        }

        return ds.asDatasetGraph();

    }

    private int sumStatements(ArrayList<Graph> dga, String graphName) {

        int sum = 0;
        for (Graph g : dga)
        {
            sum += g.size();

        }
        return sum;


    }

    private void preprocess(JSONObject jobj, Graph g) {

        generateID();
        jobj.put("@id","http://dialsws.sti2.at/entity/E"+GraphDBClient.lastIdentifier);
        Node sid = NodeFactory.createURI ((String) ((JSONObject) jobj).get("@id"));
        Node pid = NodeFactory.createURI("http://dialsws.sti2.at/vocab/id");
        Node oid = NodeFactory.createLiteralByValue(GraphDBClient.lastIdentifier, XSDDatatype.XSDinteger);
        Triple t = new Triple(sid, pid, oid);
        //Quad qid = new Quad(NodeFactory.createURI(graphName),sid,pid,oid);
        g.add(t);

    }

    private void generateID() {

        client.getLastIdentifier();
    }

    private Node createNode(RDFDataset.Node object) {


        if (object.get("type").equals("IRI"))
            return NodeFactory.createURI(object.getValue().trim());
        else if (object.get("type").equals("literal"))
            return NodeFactory.createLiteral(object.getValue().trim());
        else if (object.get("type").equals("blank node"))
            return NodeFactory.createBlankNode(object.getValue().trim());
        else
            return null;

    }


   public void pushToKB(String fileName, String graphName) throws IOException, ParseException, URISyntaxException, JsonLdError {
        convertToNQuad(readFromFile(fileName), graphName);

    }

    public void cleanKB(ArrayList<WebSite> websites) throws URISyntaxException {

        ArrayList<String> graphNames = new ArrayList<String>();
        for (WebSite webSite : websites)
            graphNames.add(webSite.getGraphName());

        client.deleteAllGraphs(graphNames);

    }
}
