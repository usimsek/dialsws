package org.sti2.dialsws.stm;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFDataMgr;
import org.sti2.at.dialsws.client.GraphDBClient;
import org.sti2.at.dialsws.client.StanbolClient;

import java.io.IOException;
import java.io.StringWriter;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;

public class KBEntityCollector {

    private GraphDBClient gdclient;
    private StanbolClient sClient;

    public KBEntityCollector (String graphDBApiUri, String stanbolApiUri) throws URISyntaxException {
        gdclient = new GraphDBClient(graphDBApiUri);
        sClient = new StanbolClient(stanbolApiUri);

    }

    public Model collectEntities(URI graphURI) throws URISyntaxException {

        return gdclient.pullEntities(graphURI);

    }


    public void pushEntities(Model model) throws IOException, URISyntaxException {


        StringWriter out = new StringWriter();
        RDFDataMgr.write(out, model, Lang.TTL);

        HashMap<String,String> parameters = new HashMap<String, String>();
        parameters.put("update","true");
        sClient.loadEntities(out.toString(), "/entityhub/site/seefeld-com-entities/entity", parameters);


    }
}
