package org.sti2.dialsws.stm;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import static org.junit.Assert.*;

public class KBEntityCollectorTest {
    KBEntityCollector ec;
    @Before
    public void setUp() throws Exception {
    ec = new KBEntityCollector("http://graphdb.sti2.at:8080", "http://dialsws.xyz:8080");
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void collectEntities() throws Exception {

        assertEquals(ec.collectEntities(new URI("http://seefeld.com")).size(), 117608);

    }

    @Test
    public void pushEntities() throws URISyntaxException, IOException {
        ec.pushEntities(ec.collectEntities(new URI("http://seefeld.com")));
    }


}