package org.sti2.at.dialsws.client;

import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.jena.query.Query;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QueryFactory;
import org.apache.jena.query.ResultSet;
import org.apache.jena.sparql.modify.UpdateProcessRemote;
import org.apache.jena.sparql.util.Context;
import org.apache.jena.update.UpdateFactory;
import org.apache.jena.update.UpdateRequest;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;

import org.apache.jena.rdf.model.*;

public class GraphDBClient extends GenericClient {

    public static long lastIdentifier = -1;
    Model entities;

    public GraphDBClient(String apiUri) throws URISyntaxException {
        super(apiUri);

        entities = ModelFactory.createDefaultModel();
    }


    public void pushNquadsToGraphDB (String nquads) throws URISyntaxException, IOException {
        URI requestUri = new URIBuilder(getApiUri()).setPath("/repositories/dialsws/statements").build();
        HttpPost postReq = new HttpPost(requestUri);
        postReq.addHeader("Content-Type","text/x-nquads");
        postReq.setEntity(new StringEntity(nquads));

        String ret = executeHttpClient(postReq);

        int trialCount = 0;
        while (ret != null && ret.equals("error") && trialCount <= 5)
        {
            ret = executeHttpClient(postReq);
            trialCount++;

            if (trialCount == 2)
                System.out.println("ERror :"+ postReq.getEntity());
        }

    }

    public void getLastIdentifier()
    {
        if (lastIdentifier == -1) {
            String queryString = "SELECT (COALESCE(MAX(?identifier),0) as ?maxIdentifier)\n" +
                    "WHERE\n" +
                    "  { ?s <http://dialsws.sti2.at/vocab/id> ?identifier  }\n" +
                    "\n";
            Query query = QueryFactory.create(queryString);

            try {
                QueryExecution qexec = QueryExecutionFactory.sparqlService(new URIBuilder(getApiUri()).setPath("/repositories/dialsws").build().toString(), query);
                ResultSet results = qexec.execSelect();

                if (results.hasNext()) {
                    lastIdentifier = results.next().getLiteral("maxIdentifier").getInt()+1;

                }


            } catch (URISyntaxException e) {
                e.printStackTrace();
            }

        }
        else
        {
            lastIdentifier ++;
        }
    }

    public void deleteAllGraphs(ArrayList<String> graphNames) throws URISyntaxException {

        for (String graphName : graphNames)
        {
            String q = "DROP GRAPH <"+graphName+">";

            UpdateRequest ur = UpdateFactory.create(q);

            UpdateProcessRemote upr = new UpdateProcessRemote(ur,new URIBuilder(getApiUri()).setPath("/repositories/dialsws/statements").build().toString(), Context.emptyContext);
            upr.execute();

        }
    }

    public Model pullEntities(URI graphURI) throws URISyntaxException {

        String sparqlQuery="PREFIX schema: <http://schema.org/>\n" +
                "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n" +
                "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\n" +
                " CONSTRUCT \n" +
                "{\n" +
                "    ?s rdf:type ?type.\n" +
                "    ?s rdfs:label ?name.\n" +
                "\n" +
                "}\n" +
                "WHERE\n" +
                "{\n" +
                "\tGRAPH <http://seefeld.com>\n" +
                "    {\n" +
                "    \t?s rdf:type ?type.\n" +
                "    \t?s schema:name ?name\n" +
                "\t} FILTER (!isBLANK(?s)).\n" +
                "} ";

        URI requestUri = new URIBuilder(getApiUri()).setPath("/repositories/dialsws").build();
        QueryExecution qexec = QueryExecutionFactory.sparqlService(requestUri.toString(),QueryFactory.create(sparqlQuery));

        qexec.execConstruct(entities);

        return entities;
    }
}
