package org.sti2.at.dialsws.client;

import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.jena.rdf.model.Model;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;

public class StanbolClient extends GenericClient {
    public StanbolClient(String apiUri) throws URISyntaxException {
        super(apiUri);
    }

    public void loadEntities(String entities, String endpoint, HashMap<String,String> parameters) throws URISyntaxException, IOException {

        URIBuilder uriBuilder = new URIBuilder(getApiUri()).setPath(endpoint);

        for (String key : parameters.keySet())
            uriBuilder.addParameter(key,parameters.get(key));

        URI requestUri = uriBuilder.build();
        HttpPost postReq = new HttpPost(requestUri);
        postReq.addHeader("Content-Type", "text/turtle");



        postReq.setEntity(new StringEntity(entities));
        String ret = executeHttpClient(postReq);

        int trialCount = 0;
        while (ret != null && ret.equals("error") && trialCount <= 5)
        {
            ret = executeHttpClient(postReq);
            trialCount++;

            if (trialCount == 2)
                System.out.println("ERror :"+ postReq.getEntity());
        }

    }
}
