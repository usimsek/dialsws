package org.sti2.at.dialsws.client;

import com.sun.deploy.util.SessionState;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import java.io.FileWriter;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.util.Date;

public abstract class GenericClient {

    private final URI apiUri;
    private ResponseHandler<String> responseHandler;
    private HttpClient client;

    public GenericClient(String apiUri) throws URISyntaxException {
        this.apiUri = new URI(apiUri);
        setResponseHandler();
        client = HttpClients.createDefault();
    }

    protected String executeHttpClient(HttpUriRequest req) throws IOException {
        try {


            return client.execute(req, responseHandler);
        }catch (ClientProtocolException cex){
            if (req.getMethod().equals("POST")) {
                FileWriter fileWriter = new FileWriter("errorlog-"+new Date().getTime()+".txt");
                fileWriter.write(IOUtils.toString(((HttpPost)req).getEntity().getContent(), StandardCharsets.UTF_8));
                fileWriter.flush();
            }
            cex.printStackTrace();
            return  "error";
        }
    }




    public URI getApiUri ()
    {
        return apiUri;
    }

    private void setResponseHandler() {

        this.responseHandler = new ResponseHandler<String>() {

            public String handleResponse(
                    final HttpResponse response) throws IOException {
                int status = response.getStatusLine().getStatusCode();
                if (status >= 200 && status < 300) {
                    HttpEntity entity = response.getEntity();
                    return entity != null ? EntityUtils.toString(entity) : null;
                } else {
                    throw new ClientProtocolException("Unexpected response status: " + status);
                }
            }
        };
    }
}
