package org.sti2.at.dialsws.client;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;

public class SemantifyItClient extends GenericClient {


    public SemantifyItClient(String apiURI) throws URISyntaxException {
      super(apiURI);

    }
    public SemantifyItClient() throws URISyntaxException {
        super("https://semantify.it/");
    }


    public JSONArray getAnnotationHashList(String apiKey) throws IOException {

        try
        {
            URI requestURI = new URIBuilder(getApiUri()).setPath("/api/annotation/list/"+apiKey).build();

            HttpGet getReq = new HttpGet(requestURI);
            getReq.addHeader("Accept","application/json");
            // Create a custom response handler

            JSONParser parser = new JSONParser();
            JSONArray hashes = (JSONArray) parser.parse(executeHttpClient(getReq));

            return hashes;

        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


    public ArrayList<JSONObject> getSingleAnnotation(String UID) throws IOException {
        ArrayList<JSONObject> annotations = new ArrayList<JSONObject>();

        try
        {
            URI requestURI = new URIBuilder(getApiUri()).setPath("/api/annotation/short/"+UID).build();
            HttpGet getReq = new HttpGet(requestURI);
            getReq.addHeader("Accept","application/json");

            JSONParser parser = new JSONParser();
            Object json = parser.parse(executeHttpClient(getReq));

            if (json instanceof  JSONObject)
                annotations.add((JSONObject) json);
            else if (json instanceof  JSONArray)
            {
                for (Object jsonObject : (Iterable<JSONObject>) ((JSONArray) json)) {

                    if (jsonObject instanceof  JSONObject)
                        annotations.add((JSONObject) jsonObject);

                }
            }

        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (ClientProtocolException cex){
            cex.printStackTrace();
            System.err.println("Error :" + UID);
            return null;
        }


        return annotations;
    }

}
