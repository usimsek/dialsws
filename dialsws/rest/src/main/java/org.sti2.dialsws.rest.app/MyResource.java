package org.sti2.dialsws.rest.app;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.sti2.dialsws.nlg.NLGenerator;

/**
 * Root resource (exposed at "myresource" path)
 */
@Path("home")
public class MyResource {

    /**
     * Method handling HTTP GET requests. The returned object will be sent
     * to the client as "text/plain" media type.
     *
     * @return String that will be returned as a text/plain response.
     */
    @GET
    @Path("hello")
    @Produces(MediaType.TEXT_PLAIN)
    public String getIt() {

        NLGenerator generator = new NLGenerator();
        return generator.generateSentenceFromTriple("http://dbpedia.org/resource/Albert_Einstein", "http://dbpedia.org/ontology/birthPlace", "http://dbpedia.org/resource/Ulm");


    }
}