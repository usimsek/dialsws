package org.sti2.dialsws.nlg;

import com.hp.hpl.jena.graph.NodeFactory;
import com.hp.hpl.jena.graph.Triple;
import org.aksw.triple2nl.TripleConverter;
import org.dllearner.kb.sparql.SparqlEndpoint;
import simplenlg.framework.DocumentElement;
import simplenlg.framework.NLGFactory;
import simplenlg.lexicon.XMLLexicon;
import simplenlg.realiser.english.Realiser;


public class NLGenerator {

    public NLGenerator() {
    }

    public String generateSentenceFromTriple(String s, String p, String o) {

        Triple t = Triple.create(NodeFactory.createURI(s), NodeFactory.createURI(p), NodeFactory.createURI(o));
        SparqlEndpoint endpoint = SparqlEndpoint.getEndpointDBpedia();
        TripleConverter converter = new TripleConverter(endpoint);
        DocumentElement docEl = new NLGFactory(new XMLLexicon()).createSentence(converter.convertTriple(t));
        Realiser realiser = new Realiser(new XMLLexicon());
        String text = realiser.realise(docEl).getRealisation();
        return text;


    }
}
